module gowatcher/go_spider

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/Shopify/sarama v1.19.0
	github.com/antchfx/htmlquery v1.2.0 // indirect
	github.com/antchfx/xmlquery v1.1.0 // indirect
	github.com/antchfx/xpath v1.1.1 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/golang/groupcache v0.0.0-20191027212112-611e8accdfc9 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/robfig/cron v1.2.0
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/temoto/robotstxt v1.1.1 // indirect
	github.com/tidwall/gjson v1.3.4
	gopkg.in/yaml.v2 v2.2.5
)
